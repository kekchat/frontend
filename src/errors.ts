export interface APIError {
    type: string;
    code: number;
    message: string;
    [dataEntry: string]: any;
}