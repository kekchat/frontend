import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, finalize, map, share } from 'rxjs/operators';
import { APIError } from 'src/errors';
import { User } from '../models/user.model';

@Injectable()
export class UsersService {
  constructor(
    private http: HttpClient
  ) { }

  private error = new BehaviorSubject<APIError|null>(null);
  readonly error$ = this.error.asObservable();

  private loading = new BehaviorSubject<boolean>(false);
  readonly loading$ = this.loading.asObservable();

  private users = new BehaviorSubject<User[]>([]);
  readonly users$ = this.users.asObservable();

  fetch(): Observable<User[]> {
    this.loading.next(true);

    return this.http.get<{ rows: User[] }>('/api/users')
      .pipe(
        share(),
        finalize(() => {
          this.loading.next(false);
        }),
        map(({ rows }) => {
          this.users.next(rows);
          this.error.next(null);

          return rows;
        }),
        catchError((error: APIError) => {
          this.users.next([]);
          this.error.next(error);

          throw error;
        })
      );
  }
}
