import { Routes } from '@angular/router';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';

export const routes: Routes = [
    {
        path: '',
        component: WelcomePageComponent,
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './modules/auth/auth.module#AuthModule'
    },
    {
        path: 'media',
        loadChildren: './modules/media/media.module#MediaModule'
    }
];