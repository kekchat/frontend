import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { APIError } from 'src/errors';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  form = this.fb.group({
    username: '',
    email: '',
    password: '',
    passwordAgain: ''
  });

  loading = false;
  error: APIError|null = null;

  onSubmit() {
    this.loading = true;
    this.authService.register({
      username: this.form.get('username').value,
      email: this.form.get('email').value,
      password: this.form.get('password').value
    }).pipe(
      finalize(() => this.loading = false)
    ).subscribe({
      next: () => this.router.navigateByUrl('/auth/login'),
      error: (error: APIError) => this.error = error
    });
  }
}
