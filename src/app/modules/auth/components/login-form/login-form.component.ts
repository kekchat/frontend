import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize, catchError } from 'rxjs/operators';
import { APIError } from 'src/errors';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  form = this.fb.group({
    login: '',
    password: ''
  });

  loading = false;
  error: APIError|null = null;

  onSubmit() {
    this.loading = true;
    this.authService.login(
      this.form.get('login').value,
      this.form.get('password').value
    ).pipe(
      finalize(() => this.loading = false)
    ).subscribe({
      next: () => this.router.navigateByUrl('/'),
      error: (error: APIError) => this.error = error
    });
  }
}
