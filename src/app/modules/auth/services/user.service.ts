import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, finalize, share, tap } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';
import { APIError } from 'src/errors';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient
  ) { }

  private error = new BehaviorSubject<APIError|null>(null);
  readonly error$ = this.error.asObservable();

  private loading = new BehaviorSubject<boolean>(false);
  readonly loading$ = this.loading.asObservable();

  private user = new BehaviorSubject<User|null>(null);
  readonly user$ = this.user.asObservable();

  fetch(): Observable<User> {
    this.loading.next(true);

    return this.http.get<User>('/api/users/self')
      .pipe(
        share(),
        finalize(() => {
          this.loading.next(false);
        }),
        tap(user => {
          this.user.next(user);
          this.error.next(null);
        }),
        catchError((error: APIError) => {
          this.user.next(null);
          this.error.next(error);

          throw error;
        })
      );
  }

  clear() {
    this.error.next(null);
    this.loading.next(false);
    this.user.next(null);
  }
}