import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/modules/auth/services/user.service';
import { environment } from 'src/environments/environment';
import { APIError } from 'src/errors';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private userService: UserService
  ) {
    const token = localStorage.getItem(environment.authToken);
    if (token) {
      this.setToken(token);
      setTimeout(() => {
        this.userService.fetch()
          .subscribe({
            error: (error: APIError) => {
              if (error.code === 403) {
                this.removeToken();
              }
            }
          });
      });
    }
  }

  private token: string|null = null;

  getToken() { return this.token; }

  private setToken(token: string) {
    this.token = token;
    localStorage.setItem(environment.authToken, token);
  }

  private removeToken() {
    this.token = null;
    localStorage.removeItem(environment.authToken);
  }

  login(login: string, password: string) {
    this.logout();
    return this.http.post<{ token: string }>('/api/login', {
      login,
      password
    }).pipe(
      map(({ token }) => {
        this.setToken(token);
        this.userService.fetch().subscribe();

        return null;
      })
    );
  }

  register(options: { username: string, email: string, password: string }) {
    const { username, email, password } = options;

    this.logout();
    return this.http.post<void>('/api/register', {
      username,
      email,
      password
    });
  }

  logout() {
    this.removeToken();
    this.userService.clear();
  }
}
