import { Routes } from '@angular/router';
import { SelfPageComponent } from './components/self-page/self-page.component';

export const routes: Routes = [
    {
        path: 'self',
        component: SelfPageComponent
    }
];