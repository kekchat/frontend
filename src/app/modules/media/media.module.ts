import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlayerReprComponent } from './components/player-repr/player-repr.component';
import { PlayerComponent } from './components/player/player.component';
import { routes } from './media.routing';
import { SelfPageComponent } from './components/self-page/self-page.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PlayerReprComponent, PlayerComponent, SelfPageComponent]
})
export class MediaModule { }
