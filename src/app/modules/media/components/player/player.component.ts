import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-player',
  template: `
    <app-player-repr
      [stream]="stream">
    </app-player-repr>
  `
})
export class PlayerComponent {
  @Input() stream: MediaStream;
}
