import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-self-page',
  templateUrl: './self-page.component.html',
  styleUrls: ['./self-page.component.css']
})
export class SelfPageComponent implements OnInit, OnDestroy {
  stream: MediaStream|null = null;

  async ngOnInit() {
    this.stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
  }

  ngOnDestroy() {
    this.stream.stop();
  }
}
