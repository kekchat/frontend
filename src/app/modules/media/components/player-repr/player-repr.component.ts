import { Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-player-repr',
  templateUrl: './player-repr.component.html',
  styleUrls: ['./player-repr.component.css']
})
export class PlayerReprComponent implements OnChanges {
  @Input() stream: MediaStream;

  @ViewChild('video') video: ElementRef<HTMLVideoElement>;

  ngOnChanges(changes: SimpleChanges) {
    if (changes["stream"] && this.video) {
      this.video.nativeElement.load();
    }
  }
}
