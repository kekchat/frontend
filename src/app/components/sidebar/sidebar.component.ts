import { Component } from '@angular/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { UserService } from 'src/app/modules/auth/services/user.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [
    UsersService
  ]
})
export class SidebarComponent {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) { }

  user$ = this.userService.user$;

  onLogout() {
    this.authService.logout();
  }
}
