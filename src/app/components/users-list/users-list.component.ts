import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  constructor(
    private usersService: UsersService
  ) { }

  users$ = this.usersService.users$;

  ngOnInit() {
    this.onReload();
  }

  onReload() {
    this.usersService.fetch().subscribe();
  }

  trackUserBy(user: User): string {
    return user.id;
  }
}
